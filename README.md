# Software Abstract

Originally created by Jiancheng Yang, Rui Shi, Bingbing Ni, Bilian Ke, the program trains a neural network with selected medical images and evaluates its ability to accurately classify new,
unseen testing data. It is worth noting that although the network uses medical image data and performs quite well, it is not intended for clinical use. This does not undermine its value. Automated methods
in the medical field are not yet widespread or adequate. This is a stepping stone for further development. This project specifically seeks to accelerate the train/test process with parallelization.

# Installation

The code only requires Python environments for machine learning. The dependencies are as follows:

* Python 3 (Anaconda 3.6.3 specifically)
* PyTorch\==0.3.1
* numpy\==1.18.5
* pandas\==0.25.3
* scikit-learn\==0.22.2
* tqdm

Before anything else, make sure to have Anaconda installed on your dev node. 
Follow this guide if you do not have it already: https://www.youtube.com/watch?v=g0rGb6QqBPo.
The command `make envs` should install all need dependencies. Otherwise, use `pip install package` if individual installs are required. Make sure to activate the environment. Additionally, testing should be doneon a GPU enabled node that is not dev-intel14-k20. Nodes that lack this functionality can still be tested on, but they will take too long and face risk of breaking during execution. dev-intel14 is not a valid 
testing choice either. 

# Example Code

A quick demonstration of the code can simply be performed by entering:

* `make envs`
* `make clean`
* `make`
* `make s_test` for serial OR `make p_test` for parallel

The input file can be changed by editing the `FILE` variable in the makefile. Be warned that the files differ in size considerably. The default is BreastMNIST, the smallest dataset. In ascending order of size,
the usable input datasets are:

* breastmnist
* retinamnist
* pneumoniamnist
* organmnist_coronal
* organmnist_sagittal
* dermamnist
* organmnist_axial
* octmnist
* chestmnist
* pathmnist

If one wants to exert more precise control of program inputs, use the following commands in succession:

* `python download.py --data_name xxxmnist --input_root <path/to/input/folder>` where xxxmnist is one of the above 10 choices
* `python train.py --data_name xxxmnist --input_root <path/to/input/folder> --output_root <path/to/output/folder> --num_epoch xxx --download bool` where xxxmnist is the desired input, xxx is the number of epoch
iterations (10-20 is preferable, but more or less can be done depending on file size), and bool is a True or False flag.

The `download.py` script was made to separate the dataset download from the rest of the program as insurance that the desired dataset is already loaded before any timing is done. Additionally, some of the 
datasets are quite large, so that is why they are not provided in the repository and must be downloaded afterwards.

# Submission script

Simply type `sbatch timing.sh` to submit a SLURM job to the HPCC. `cat` the resulting slurm output to see the time taken on a node.

# References

If necessary, the data sets can be downloaded via the free accesses:

* https://zenodo.org/record/4269852#.YFqz9dwpBhE
* https://drive.google.com/drive/folders/1Tl_SP-ffDQg-jDG_EWPlWKgZTmGbvFXU
* https://pan.baidu.com/share/init?surl=bgPbESbLOlUSu4QC-4O46g (note that this website is printed in Mandarin, the code is gx6i)

Original repository:
* https://github.com/MedMNIST/MedMNIST.git

Original research Paper
* https://arxiv.org/abs/2010.14925

Original project page:
* https://medmnist.github.io/#citation

#!/bin/bash
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem 4gb
#SBATCH --time 00:90:00

module load Conda/3
conda activate ./envs

make clear
make
for n in {1..3};
do	time srun make s_test
done;

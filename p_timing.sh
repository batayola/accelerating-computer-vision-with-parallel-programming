#!/bin/bash
#SBATCH -N 1
#SBATCH -c 5
#SBATCH --gres=gpu:3
#SBATCH --mem 4gb
#SBATCH --time 00:30:00

module load Conda/3
module load CUDA
conda activate ./envs

make clear
make
for n in {1..3};
do	time srun make p_test
done;

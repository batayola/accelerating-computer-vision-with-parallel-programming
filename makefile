all: download

FILE:=breastmnist

envs: requirements.yml
	conda env create --prefix ./envs -f requirements.yml

download: download.py
	python download.py --data_name $(FILE) --input_root input
	
s_test: download
	python s_train.py --data_name $(FILE) --input_root input --output_root output --num_epoch 10 --download True

p_test: download
	python p_train.py --data_name $(FILE) --input_root input --output_root output --num_epoch 10 --download True

clean:
	rm -r input
	rm -r output
